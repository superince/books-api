# Google Book API
*A simple book search and bookmarking application*

**Tech Used**
- Adonisjs.js
- Redis(Caching)
- Database(Mysql)
- Api Testing(Postman)

**Installation Steps**

1. Install the dependencies

    `npm install`

2. Copy env file from .env.example to .env and fill up required variables

    `cp .env.example .env`

3. Migrate tables to database

    `node ace migration:run`

4. Run Application

    `npm run dev`

