import { schema, CustomMessages, rules } from "@ioc:Adonis/Core/Validator";
import type { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";

export default class GoogleBookValidator {
  constructor(protected ctx: HttpContextContract) {}

  public schema = schema.create({
    title: schema.string.optional({}, [
      rules.requiredIfNotExistsAll(["author", "keyword"]),
    ]),
    author: schema.string.optional({}, [
      rules.requiredIfExistsAll(["title", "keyword"]),
    ]),
    keyword: schema.string.optional({}, [
      rules.requiredIfExistsAll(["author", "title"]),
    ]),

    page: schema.number.optional(),
    limit: schema.number.optional([rules.range(1, 40)]),
  });

  public messages: CustomMessages = {
    "title.requiredIfNotExistsAll": "Any one of the field is required(title, author or keyword)",
    "limit.range": "Range Limit is 1 to 20",
  };
}
