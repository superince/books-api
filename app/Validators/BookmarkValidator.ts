import { schema, CustomMessages } from '@ioc:Adonis/Core/Validator'
import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class BookmarkValidator {
  constructor(protected ctx: HttpContextContract) {}

 
  public schema = schema.create({
    googleBookId: schema.string({ trim: true }),
  })

  public messages: CustomMessages = {
    'googleBookId.required': 'Google Book Id is required.',
  }
}
