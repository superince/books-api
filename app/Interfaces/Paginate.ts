type Nullable<T> = T | null;

export interface PaginateParams {
  readonly page: number;
  readonly limit?: number;
}

export interface PaginationMeta {
  page: number;
  perPage: number;
  totalCount: number;
  links: [
    { self: string },
    { first: string },
    { previous: Nullable<string> },
    { next: Nullable<string> },
    { last: string }
  ];
}
