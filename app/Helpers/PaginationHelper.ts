import { PaginationMeta } from "App/Interfaces/Paginate";

export default class Pagination {
  private page: number;
  private pageLimit;
  private queryParams;

  constructor(page: number, pageLimit: number, queryParams?: {}) {
    this.pageLimit = pageLimit || 20;
    this.page = Number(page) || 1;
    this.queryParams = queryParams;
  }

  public getOffset(): number {
    return (this.page - 1) * this.pageLimit;
  }

  public getPageLimit(): number {
    return this.pageLimit;
  }

  public getPageNos(totalData): PaginationMeta {
    const totalPages = Math.ceil(totalData / this.pageLimit);
    const first = 1;
    const last = totalPages || first;
    const next = this.page < totalPages ? this.page + 1 : null;
    const previous = this.page > 1 ? this.page - 1 : null;
    const self = this.page;
    
    const paginationMeta: PaginationMeta = {
      page: self,
      perPage: Number(this.pageLimit),
      totalCount: Number(totalData),
      links:[
        { self:`/books/search/?${new URLSearchParams({ page: self, ...this.queryParams }).toString()}`},
        { first: `/books/search/?${new URLSearchParams({ page: first, ...this.queryParams }).toString()}` },
        { previous: previous ? `/books/search/?${new URLSearchParams({ page: previous, ...this.queryParams }).toString()}` : null },
        { next: next ? `/books/search/?${new URLSearchParams({ page: next, ...this.queryParams }).toString()}` : null },
        { last: `/books/search/?${new URLSearchParams({ page: last, ...this.queryParams }).toString()}` }
      ]
    };

    return paginationMeta;
  }
}
