import { paginationLimit } from "Config/app";
import { GoogleBookDTO } from "App/Interfaces/GoogleBook";
import { PaginateParams } from "App/Interfaces/Paginate";
import { searchBook } from "App/API/GoogleBookApi";

import GoogleBookValidator from "App/Validators/GoogleBookValidator";

export default class GoogleBooksController {
  public async index({ request }) {
    await request.validate(GoogleBookValidator);
    const filters: GoogleBookDTO = request.only(["title", "author", "keyword"]);

    let paginationParams: PaginateParams = {
      page: request.input("page", 1),
      limit: request.input("limit", paginationLimit),
    };

    const booksData = await searchBook(filters, paginationParams);

    return { data: booksData.data, _metadata: booksData.paginationMeta };
  }
}
