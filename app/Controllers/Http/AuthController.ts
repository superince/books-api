import Hash from "@ioc:Adonis/Core/Hash";
import User from "App/Models/User";
import CreateUserValidator from "App/Validators/CreateUserValidator";

export default class AuthController {
  public async handleSignup({ request }) {
    try {
      const data = await request.validate(CreateUserValidator);
      const user = await User.create(data);
      return user;
    } catch (err) {
      throw err;
    }
  }

  public async handleLogin({ auth, request }) {
    const { email, password } = request.all();
    try {
      // Attempt login
      await auth.attempt(email, password);
      let user = await User.findByOrFail("email", email);

      // Verify password
      await Hash.verify(user.password, password);

      let token = await auth.use("api").generate(user, {
        expiresIn: "60 mins",
      });

      return token;
    } catch (err) {
      throw err;
    }
  }
}
