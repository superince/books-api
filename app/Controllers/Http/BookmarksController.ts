import { getBookDetails } from "App/API/GoogleBookApi";
import { PaginateParams } from "App/Interfaces/Paginate";
import UserBookmark from "App/Models/Bookmark";
import BookmarkValidator from "App/Validators/BookmarkValidator";
import { paginationLimit } from "Config/app";

export default class BookmarksController {
  public async index({ auth, request }) {
    const user = auth.use("api").user;
    const userId = user.id;

    let paginationParams: PaginateParams = {
      page: request.input("page", 1),
      limit: request.input("limit", paginationLimit),
    };

    try {
      const userBookmarks = await UserBookmark.query()
        .where("userId", userId)
        .paginate(paginationParams.page, paginationParams.limit);
      return userBookmarks;
    } catch (err) {
      throw err;
    }
  }

  public async store({ request, response, auth }) {
    await request.validate(BookmarkValidator);

    const user = auth.use("api").user;
    const userId = user.id;
    const googleBookId = request.input("googleBookId");

    let data;
    try {
      // Check if googleBookId exists
      data = await getBookDetails(googleBookId);
    } catch (error) {
      return response.status(404).send({
        message: "Invalid Google book id",
      });
    }

    // Check if googleBookId is already bookmarked
    const checkBookmarkExists = await UserBookmark.query()
      .where("userId", userId)
      .where("googleBookId", googleBookId);

    if (checkBookmarkExists.length) {
      return response.status(409).send({
        message: "You have already bookmarked this book.",
      });
    }

    // Create bookmark
    const newBookmark = await UserBookmark.create({
      userId,
      googleBookId,
      data: JSON.stringify(data),
    });
    newBookmark.data = data;

    return response.status(200).send({
      message: "Successfully created bookmark.",
      data: newBookmark,
    });
  }

  public async destroy({ request, response, auth }) {
    const user = auth.use("api").user;
    const userId = user.id;

    const googleBookId = request.param("googleBookId");

    // Check if bookmark exists
    const checkBookmarksExists = await UserBookmark.query()
      .where("userId", userId)
      .where("googleBookId", googleBookId);

    if (!checkBookmarksExists.length) {
      return response.status(404).send({
        message: "Invalid Google Bookmark Id",
      });
    }

    try {
      await UserBookmark.query()
        .where("userId", userId)
        .andWhere("googleBookId", googleBookId)
        .delete();

      return response.status(200).send({
        message: "Successfully deleted bookmark.",
      });
    } catch (err) {
      throw err;
    }
  }
}
