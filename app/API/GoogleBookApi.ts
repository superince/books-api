import Pagination from "App/Helpers/PaginationHelper";
import { GoogleBookResponse, GoogleBookDTO } from "App/Interfaces/GoogleBook";
import { PaginateParams, PaginationMeta } from "App/Interfaces/Paginate";
import { googleBooksKey } from "Config/app";
import Redis from "@ioc:Adonis/Addons/Redis";
import axios from "axios";

export const searchBook = async (
  filters: GoogleBookDTO,
  paginationParams: PaginateParams
) => {
  let query = "";

  if (filters.keyword) {
    query += `${filters.keyword}+`;
  }

  if (filters.title) {
    query += `intitle:${filters.title}`;
  }

  if (filters.author) {
    query += `inauthor:${filters.author}`;
  }

  const pagination = new Pagination(
    paginationParams.page,
    paginationParams.limit || 40,
    filters
  );
  const startIndex = pagination.getOffset();

  const bookSearchParams = {
    q: query,
    key: googleBooksKey,
    maxResults: paginationParams.limit,
    startIndex,
  };

  try {
    const booksResponse = await axios.get(
      "https://www.googleapis.com/books/v1/volumes",
      {
        params: bookSearchParams,
      }
    );

    const paginationMeta: PaginationMeta = pagination.getPageNos(
      booksResponse.data.totalItems
    );
    const data: GoogleBookResponse[] = booksResponse.data.items;

    return { data, paginationMeta };
  } catch (err) {
    throw err;
  }
};

export const getBookDetails = async (id: string) => {
  try {
    const redisGoogleBookKey = `googleBookId:${id}`;
    const redisRes = await Redis.get(redisGoogleBookKey);

    if (redisRes) {
      return JSON.parse(redisRes);
    }

    const bookDetailsRes = await axios.get(
      `https://www.googleapis.com/books/v1/volumes/${id}`,
      {
        params: {
          key: googleBooksKey,
        },
      }
    );

    Redis.setex(redisGoogleBookKey, 1800, JSON.stringify(bookDetailsRes.data));

    return bookDetailsRes.data;
  } catch (err) {
    throw err;
  }
};
