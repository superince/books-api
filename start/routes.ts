import Route from "@ioc:Adonis/Core/Route";

Route.group(() => {
  Route.group(() => {
    Route.post("/signup", "AuthController.handleSignup");
    Route.post("/login", "AuthController.handleLogin");
  }).prefix("auth");

  Route.get("/books/search", "GoogleBooksController.index");

  Route.group(() => {
    Route.group(() => {
      Route.get("/", "BookmarksController.index");
      Route.post("/", "BookmarksController.store");
      Route.delete("/:googleBookId", "BookmarksController.destroy");
    }).prefix("bookmarks");
  }).middleware("auth:api");
});
